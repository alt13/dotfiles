#!/bin/bash
# cliphist list | tofi | cliphist decode | wl-copy


# Temporary file to store the selected item
tmpfile=$(mktemp)

# Fetch the clipboard history using Cliphist

# cliphist list | tofi | cliphist decode | wl-copy
clipboard_items=$(cliphist list | tofi)

# clipboard_items=$(cliphist list | tofi -dmenu -p "Clipboard")

# If an item is selected, store it in the temporary file
if [ -n "$clipboard_items" ]; then
    echo -n "$clipboard_items" > "$tmpfile"

    # Determine if the selected item is an image (check for image magic numbers)
    if file "$tmpfile" | grep -qE 'image|bitmap'; then
        # Copy the image to the clipboard using wl-clipboard
        wl-copy < "$tmpfile"
    else
        # Copy text to the clipboard
        echo -n "$clipboard_items" | wl-copy
    fi
fi

# Clean up the temporary file
rm "$tmpfile"


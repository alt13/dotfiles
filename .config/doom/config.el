(setq user-full-name "Alt13"
      user-mail-address "john@doe.com")

(setq doom-theme 'doom-monokai-classic)
;; (custom-theme-set-faces! 'doom-monokai-classic
  ;; '(default :background "#000000"))

(setq default-frame-alist
      '((alpha . 90)))

(setq doom-font (font-spec :family "Comic Code Ligatures" :size 16)
     doom-variable-pitch-font (font-spec :family "Ubuntu" :size 16))

(use-package dashboard
  :init      ;; tweak dashboard config before loading it
  (setq dashboard-banner-logo-title "꧁ DOOM EMACS Alt13's edition ꧂")
  ;; (setq dashboard-banner-logo-title "https://gitlab.com/alt13")
  (setq dashboard-startup-banner "~/.config/doom/dash.png")  ;; use custom image as banner
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-center-content t)
  (setq dashboard-items '((recents . 10)
                          (bookmarks . 5)
                          (projects . 5)))
  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book"))))

(setq doom-fallback-buffer-name "*dashboard*")

(global-tab-line-mode t)               ;; show the tabline in the top of the frame
(setq tab-line-new-button-show nil)    ;; do not show add-new button
(setq tab-line-close-button-show nil)  ;; do not show close button

(setq display-line-numbers-type t)

(custom-set-faces'(hl-line ((t (:background nil)))))  ;; work around to disable

(use-package! org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :config
  (setq org-auto-tangle-default t))

(after! org
  (setq org-ellipsis "▾"             ; Heading... > Heading▾
        org-hide-emphasis-markers t  ; *bold* > bold
        org-superstar-headline-bullets-list '(nil "●" "■" "◆" "◉" "○" "✸" "★") ))

(after! org
  (dolist
      (face
       '((org-level-1 1.6  ultra-bold)
         (org-level-2 1.2  extra-bold)
         (org-level-3 1.1  bold)
         (org-level-4 1.1  semi-bold)
         (org-level-5 1.0  normal)
         (org-level-6 1.0  normal)
         (org-level-7 1.0  normal)
         (org-level-8 1.0  normal)))
      (set-face-attribute (nth 0 face) nil :height (nth 1 face) :weight (nth 2 face) ))
  (set-face-attribute 'org-table nil :height 1.0 :weight 'normal :foreground "#7d7e7a")
)

(custom-set-faces
    '(org-block-begin-line
        ((t (:background "#272822" :extend t))))
    '(org-block
        ((t (:background "#1f201b" :extend t))))
    '(org-block-end-line
        ((t (:background "#272822" :extend t))))
)



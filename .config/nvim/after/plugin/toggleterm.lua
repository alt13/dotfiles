-- Load toggleterm with a protected call
local status_ok, toggleterm = pcall(require, "toggleterm")
if not status_ok then
    return
end

-- Toggleterm options
toggleterm.setup({
    open_mapping = [[<a-t>]],
})


-- Foreground (doom emacs monokai classic)
vim.cmd [[highlight Headline1 guifg=#ff6188]]
vim.cmd [[highlight Headline2 guifg=#fc9867]]
vim.cmd [[highlight Headline3 guifg=#ffd866]]
vim.cmd [[highlight Headline4 guifg=#a9dc76]]
vim.cmd [[highlight Headline5 guifg=#78dce8]]
vim.cmd [[highlight Headline6 guifg=#ab9df2]]

-- Background
vim.cmd [[highlight Headline1 guibg=#313228]]
vim.cmd [[highlight Headline2 guibg=#313228]]
vim.cmd [[highlight Headline3 guibg=#313228]]
vim.cmd [[highlight Headline4 guibg=#313228]]
vim.cmd [[highlight Headline5 guibg=#313228]]
vim.cmd [[highlight Headline6 guibg=#313228]]

vim.cmd [[highlight CodeBlock guibg=#1f201b]]
vim.cmd [[highlight Dash guibg=#D19A66 gui=bold]]

require("headlines").setup {
    org = {
        headline_highlights = { "Headline1", "Headline2", "Headline3", "Headline4", "Headline5", "Headline6" },
        fat_headlines = false,  -- remove highlight paddings
    },
}

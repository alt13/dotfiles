-- Load telescope with a protected call
local status, telescope = pcall(require, "telescope")
if not status then
    return
end

local actions = require "telescope.actions"

-- Telescope options
telescope.setup {
    defaults = {
        file_ignore_patterns = { ".git/", "node_modules" },

        mappings = {
            i = {
                ["<C-q>"] = actions.close,
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<A-j>"] = actions.preview_scrolling_down,
                ["<A-k>"] = actions.preview_scrolling_up,
                ["<C-v>"] = actions.select_vertical,
                ["<C-h>"] = actions.select_horizontal,
            },
        },
    },
}

telescope.load_extension("file_browser")


require('monokai').setup{
    palette = {
        base0 = '#272822', -- telescope bg
        base1 = '#1b1c17', -- info window background
        base2 = '#272822', -- background
        base3 = '#313228', -- recommended length line / lualine color
        base4 = '#1b1c17', -- tabs color
        base5 = '#75715E', -- number line
        base6 = '#75715E', -- comments
   }
}

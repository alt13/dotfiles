--######################################
--#     ___   _    _____ __   _____    #
--#    / _ \ | |  |_   _/  | |____ |   #
--#   / /_\ \| |    | | `| |     / /   #
--#   |  _  || |    | |  | |     \ \   #
--#   | | | || |____| | _| |_.___/ /   #
--#   \_| |_/\_____/\_/ \___/\____/    #
--#      https://gitlab.com/alt13      #
--######################################

require("usr.options")
require("usr.styles")
require("usr.keybindings")
require("usr.plugins")


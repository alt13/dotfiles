--[[
{mode} {keymap} {mapped to} {options}
Modes:
    normal_mode = "n",
    insert_mode = "i",
    visual_mode = "v",
    visual_block_mode = "x",
    term_mode = "t",
    command_mode = "c",
Options:
    noremap=true    - no recursive map
    silent=true     - hide keymap output
--]]

local function map(m, k, v)
	vim.keymap.set(m, k, v, { silent = true })
end

--------------
-- [NORMAL] --
--------------

-- better window navigation
vim.api.nvim_set_keymap("n", "<C-h>", "<C-w>h", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "<C-j>", "<C-w>j", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "<C-k>", "<C-w>k", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "<C-l>", "<C-w>l", {noremap=true, silent=true})

-- resize with arrows
vim.api.nvim_set_keymap("n", "<C-Up>", ":resize -2<CR>", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "<C-Down>", ":resize +2<CR>", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "<C-Left>", ":vertical resize -2<CR>", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "<C-Right>", ":vertical resize +2<CR>", {noremap=true, silent=true})

-- clear search highlight
vim.api.nvim_set_keymap('n', '<Esc>', ':noh<Enter>', {noremap=true, silent=true})

-- regular editor
vim.api.nvim_set_keymap('v', '<C-s>', ':up<Enter>', {noremap=true}) -- write only if file was edited



--------------
-- [VISUAL] --
--------------

-- tab selected block of text left or right
vim.api.nvim_set_keymap('v', '<', '<gv', {noremap=true, silent=true})
vim.api.nvim_set_keymap('v', '>', '>gv', {noremap=true, silent=true})

-- move selected text up or down ('A' - Alt)
vim.api.nvim_set_keymap("v", "<A-j>", ":m '>+1<CR>gv=gv", {noremap=true, silent=true})
vim.api.nvim_set_keymap("v", "<A-k>", ":m '<-2<CR>gv=gv", {noremap=true, silent=true})

-- keep yanked text in buffer
vim.api.nvim_set_keymap("v", "p", '"_dP', {noremap=true, silent=true})
-- without: yank > select word to replace > put > buffect updated with selected text
-- with:    yank > select word to replace > put > buffect keeps previously yanked text



---------------
-- [PLUGINS] --
---------------

-- Bufferline (check for L and H usage)
vim.api.nvim_set_keymap("n", "<S-l>", ":bnext<CR>", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "<S-h>", ":bprevious<CR>", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "bq", ":Bdelete<CR>", {noremap=true, silent=true})  -- close tab withoug exiting vim

-- Telescope
map("n", "<C-f>r", "<CMD>Telescope oldfiles<CR>")        -- Find Recent
map("n", "<C-f>", "<CMD>Telescope find_files<CR>")       -- Find
map("n", "<leader>e", "<CMD>Telescope file_browser<CR>") -- Explorer
map("n", "<leader>fw", "<CMD>Telescope live_grep<CR>")   -- Find Word
-- :Telescope colorscheme

-- nvim-cmp (autocompletion)
    -- mapping in user/config/nvim-tree/init.lua

-- nvim-lspconfig (language server)
    -- :LspInfo - check if active
    -- :LspInstallInfo ['i' - install, 'u' - update, 'X' - delete]

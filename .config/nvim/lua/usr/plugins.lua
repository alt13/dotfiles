-- :checkhealth
-- :lazy

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
     -- Navigate vim using UA keyboard layout
    'powerman/vim-plugin-ruscmd',

 	-- Colorscheme
    "tanvirtin/monokai.nvim",

     -- Status line
 	"nvim-lualine/lualine.nvim",

 	-- Telescope
    {'nvim-telescope/telescope.nvim', tag = '0.1.3', dependencies = { 'nvim-lua/plenary.nvim' }},
 	"nvim-telescope/telescope-file-browser.nvim",

    -- Toggle a terminal
    "akinsho/nvim-toggleterm.lua",

    -- Comment
    {'numToStr/Comment.nvim', config = function() require('Comment').setup() end},

    -- Org mode
    {'nvim-treesitter/nvim-treesitter'},
    {'nvim-orgmode/orgmode', config = function()require('orgmode').setup{}end},
    {'akinsho/org-bullets.nvim', config = function() require('org-bullets').setup() end},
    {'lukas-reineke/headlines.nvim', dependencies = "nvim-treesitter/nvim-treesitter", config = true},
    "dhruvasagar/vim-table-mode",
    -- Markdown
    {"iamcco/markdown-preview.nvim", run = function() vim.fn["mkdp#util#install"]() end},
    {"iamcco/markdown-preview.nvim", run = "cd app && npm install", setup = function() vim.g.mkdp_filetypes = { "markdown" } end, ft = { "markdown" }, },

    -- Which Key
    "folke/which-key.nvim",

 	-- Syntax Highlighting and Colors --
    'norcalli/nvim-colorizer.lua',  -- color highlighter
  --
     -- Completion
    'hrsh7th/nvim-cmp',             -- completion engine
     -- plugins for cmp (also add to source in lua/user/cmp.lua)
    'hrsh7th/cmp-buffer',           -- adds current document completion
    'hrsh7th/cmp-path',             -- adds path completion
    'hrsh7th/cmp-cmdline',          -- adds cmd line completion
    'saadparwaiz1/cmp_luasnip',     -- adds snippet completion
    'L3MON4D3/LuaSnip',             -- snippets engine (autocomplete requirement)
}

require("lazy").setup(plugins, opts)


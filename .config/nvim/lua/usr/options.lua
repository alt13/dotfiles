local g = vim.g
local o = vim.o
local opt = vim.opt

-------------
-- Neovide --
-------------
opt.guifont = "Comic Code Ligatures:h12"
vim.cmd "let g:neovide_transparency=0.8"



o.termguicolors = true  -- enable all RGB colors

-- Decrease update time
o.timeoutlen = 500
o.updatetime = 200

-- Better editor UI
o.scrolloff = 8              -- number of screen lines to keep above and below the cursor
o.number = true              -- numerate lines
o.numberwidth = 2
o.relativenumber = false
o.signcolumn = "yes"         -- always show the sign column, otherwise it would shift the text each time
o.cursorline = true          -- highlight the current line
-- o.colorcolumn = '80'         -- show recommended line length

-- Better editing experience
o.expandtab = true           -- convert tabs to spaces
o.smarttab = true            -- reacts to the syntax/style of the code
o.cindent = true
o.autoindent = true
o.wrap = false               -- text moves on zoom
o.textwidth = 300
o.tabstop = 4                -- insert 4 spaces for a tab
o.shiftwidth = 4             -- 1 Tab = 4 spaces
o.softtabstop = -1           -- If negative, shiftwidth value is used
o.list = true
o.listchars = "trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂"
o.clipboard = "unnamedplus"  -- access the system clipboard
opt.mouse = "a"              -- enable mouse

-- Disable auto comment on new line
vim.api.nvim_create_autocmd("BufEnter", { callback = function()
vim.opt.formatoptions = vim.opt.formatoptions - { "c","r","o" } end, })

-- Case insensitive searching UNLESS /C or capital in search
o.ignorecase = true  -- ignore case in search patterns
o.smartcase = true   -- /The would find only "The", while /the would find "the" or "The"

-- Undo and backup options
o.backup = false
o.writebackup = false
o.undofile = false
o.swapfile = false

-- Remember 50 items in commandline history
o.history = 50

-- Better buffer splitting
o.splitright = true
o.splitbelow = true

-- Map <leader> to space
g.mapleader = " "
g.maplocalleader = " "

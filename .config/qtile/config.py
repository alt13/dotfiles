###################################### QTILE
#     ___   _    _____ __   _____    # Default config: /usr/share/doc/qtile/default_config.py
#    / _ \ | |  |_   _/  | |____ |   # Logs:           ~/.local/share/qtile/qtile.log
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   # Arco Linux qtile config https://github.com/arcolinux/arcolinux-qtile
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################
import os
import re  # bluetooth lambda
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook
from typing import List  # noqa: F401from typing import List  # noqa: F401



############
# Touchpad # https://docs.qtile.org/en/v0.23.0/_modules/libqtile/backend/wayland/inputs.html
############
from libqtile.backend.wayland import InputConfig
wl_input_rules = {"type:touchpad": InputConfig(tap=True, natural_scroll=True)}  #wayland



#############
# Modifiers #
#############
mod = 'mod4'
alt = 'mod1'
ctrl = 'control'
home = os.path.expanduser('~')



########################
# Default applications #
########################
terminal = 'kitty'
browser = 'thorium-browser'
task_manager = 'gnome-system-monitor'
file_manager = 'thunar'
editor = 'neovide'  # terminal + ' nvim'
screen_locker = './.config/qtile/scripts/i3lock_x11.sh'  # home + '/.config/qtile/scripts/i3lock.sh'

vol_snd = 'paplay .config/qtile/sounds/mac-volume-change.ogg'



################
# Key bindings #
################
keys = [
    # QTILE CONTROL
    Key([mod, ctrl], 'r', lazy.reload_config()), # wayland
    # Key([mod, ctrl], 'r', lazy.restart()),     # default
    Key([mod, ctrl], 'q', lazy.shutdown()),      # default


    # FN KEYS   https://wiki.linuxquestions.org/wiki/XF86_keyboard_symbols
    # Keyboard Brightness (my keyboard 0%, 50%, 100%) (using 'light')
    Key([], 'XF86KbdBrightnessUp',   lazy.spawn('light -s sysfs/leds/dell::kbd_backlight -A 5')),
    Key([], 'XF86MonBrightnessUp',   lazy.spawn('light -A 5'), lazy.spawn('./.config/qtile/scripts/fn_notif.sh brightness_up')),
    Key([], 'XF86MonBrightnessDown', lazy.spawn('light -U 5'), lazy.spawn('./.config/qtile/scripts/fn_notif.sh brightness_down'), ),
    # ALSA volume control
    Key([], 'XF86AudioRaiseVolume', lazy.spawn('amixer -q set Master 5%+'),    lazy.spawn(vol_snd), lazy.spawn('./.config/qtile/scripts/fn_notif.sh volume_up')),
    Key([], 'XF86AudioLowerVolume', lazy.spawn('amixer -q set Master 5%-'),    lazy.spawn(vol_snd), lazy.spawn('./.config/qtile/scripts/fn_notif.sh volume_down')),
    Key([], 'XF86AudioMute',        lazy.spawn('amixer -q set Master toggle'), lazy.spawn(vol_snd)),
    Key([], 'XF86AudioPrev',        lazy.spawn('playerctl previous')),
    Key([], 'XF86AudioPlay',        lazy.spawn('playerctl play-pause')),
    Key([], 'XF86AudioNext',        lazy.spawn('playerctl next')),
    Key([], 'XF86AudioStop',        lazy.spawn('playerctl stop')),


    # ROFI
    Key([mod], 'r',          lazy.spawn('rofi -show drun -no-fixed-num-lines')),
    Key([mod, 'shift'], 'r', lazy.spawn('python3 ./.config/rofi/scripts/alt_menu.py')),
    Key([mod], 'f',          lazy.spawn('python3 ./.config/rofi/scripts/alt_web_search.py')),
    # Key([mod], 'v',          lazy.spawn("rofi -no-fixed-num-lines -modi 'clipboard:greenclip print' -show clipboard -run-command '{cmd}'")),
    Key([alt], 'Tab',        lazy.spawn('rofi -show window')),


    # APPS
    Key([], 'Print', lazy.spawn('flatpak run org.flameshot.Flameshot gui')),
    Key([mod], 'Return', lazy.spawn(f'{terminal}')),
    Key([mod], 'b', lazy.spawn(browser)),
    #Key([mod], 'e', lazy.spawn(terminal + ' -e vifm')),
    Key([mod], 'e', lazy.spawn(file_manager)),
    Key([mod], 't', lazy.spawn(editor)),
    Key([ctrl, 'shift'], 'Escape', lazy.spawn(task_manager)),
    # Key([mod], 'Escape', lazy.spawn('setxkbmap -layout us'), lazy.spawn(screen_locker)),
    Key([alt, ctrl], 'l', lazy.spawn(screen_locker)),


    # LAYOUT
    Key([mod], 'space',      lazy.widget['keyboardlayout'].next_keyboard()),  # for custom keyboards (default mapping)
    # changing kb layout also resets key mapping ('setxkbmap') so additional script required)
    # Key([mod], 'space',      lazy.widget['keyboardlayout'].next_keyboard(),  lazy.spawn(home + '/.config/qtile/scripts/kb_mapping.sh')),  # for dedault keyboards (custom mapping)
    Key([mod], 'Tab',        lazy.next_layout()),
    Key([mod], 'm',          lazy.window.toggle_maximize()),
    Key([mod, ctrl], 'f',    lazy.window.toggle_fullscreen()),


    # WINDOW CONTROL
    Key([mod], 'c', lazy.window.kill()),
    Key([mod, 'shift'], 'x', lazy.spawn('xkill')),

    # CHANGE FOCUS
    Key([mod], 'up',    lazy.layout.up()),
    Key([mod], 'k',     lazy.layout.up()),
    Key([mod], 'down',  lazy.layout.down()),
    Key([mod], 'j',     lazy.layout.down()),
    Key([mod], 'left',  lazy.layout.left()),
    Key([mod], 'h',     lazy.layout.left()),
    Key([mod], 'right', lazy.layout.right()),
    Key([mod], 'l',     lazy.layout.right()),
    # Switch focus to specific monitor
    Key([mod], 'period', lazy.to_screen(0)),
    Key([mod], 'comma', lazy.to_screen(1)),
    # Switch focus of monitors
    # Key([mod], 'period', lazy.next_screen()),
    # Key([mod], 'comma', lazy.prev_screen()),

    # RESIZE
    Key([mod, 'shift'], 'down', lazy.layout.grow_down()),
    Key([mod, 'shift'], 'k', lazy.layout.grow_down()),
    Key([mod, 'shift'], 'up', lazy.layout.grow_up()),
    Key([mod, 'shift'], 'i', lazy.layout.grow_up()),
    Key([mod, 'shift'], 'left', lazy.layout.grow_left()),
    Key([mod, 'shift'], 'j', lazy.layout.grow_left()),
    Key([mod, 'shift'], 'right', lazy.layout.grow_right()),
    Key([mod, 'shift'], 'l', lazy.layout.grow_right()),
    # MOVE
    Key([mod, ctrl], 'down', lazy.layout.shuffle_down()),
    Key([mod, ctrl], 'k', lazy.layout.shuffle_down()),
    Key([mod, ctrl], 'up', lazy.layout.shuffle_up()),
    Key([mod, ctrl], 'i', lazy.layout.shuffle_up()),
    # move window left/right
    Key([mod, ctrl], 'left',  lazy.layout.swap_column_left()),
    Key([mod, ctrl], 'j',  lazy.layout.swap_column_left()),
    Key([mod, ctrl], 'right', lazy.layout.swap_column_right()),
    Key([mod, ctrl], 'l', lazy.layout.swap_column_right()),
    # move up > left/right
    Key([mod, 'shift', ctrl], 'left', lazy.layout.shuffle_left()),
    Key([mod, 'shift', ctrl], 'j', lazy.layout.shuffle_left()),
    Key([mod, 'shift', ctrl], 'right', lazy.layout.shuffle_right()),
    Key([mod, 'shift', ctrl], 'l', lazy.layout.shuffle_right()),
    # DEFAULT
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod, 'shift'], 'f', lazy.window.toggle_floating()),
    ]



##############
# Workspaces #
##############
groups = [
        Group('1', label=' 1 ', layout= 'monadtall'),
        Group('2', label=' 2 ', layout= 'monadtall'),
        Group('3', label=' 3 ', layout= 'monadtall'),
        Group('4', label=' 4 ', layout= 'monadtall'),
        Group('5', label=' 5 ', layout= 'monadtall'),
        Group('s', label=''), # spacer
        # Run "xprop | ebcb8bgrep 'CLASS'" to see the wm class and name of an X client.
        Group('6', label=' 6 ', layout= 'treetab', matches= [Match(wm_class=['thunderbird',])] ),]
for i in range(1, len(groups)):
    keys.append(Key([mod], str(i), lazy.group[str(i)].toscreen()))                                       # switch to workspace
    keys.append(Key([mod, 'shift'], str(i), lazy.window.togroup(str(i)), lazy.group[str(i)].toscreen())) # Send current window to workspace



#################
# Define colors #
#################
c_bar = c_border_normal = '#000000' # '#000000.0' - opacity
c_border_focus = '#4e5f60' #'#387db7' # Material-Black-Blueberry
# c_border_focus = '#666666'
c_window_name = c_widget_txt = '#AEA79F' # ubuntu Warm grey
c_active_ws = '#9c968f'
c_inactive_ws = '#333333' # ubuntu Cool grey
c_widget_icon = '#595b5c'



###########
# Layouts #
###########
layouts = [layout.Columns(margin=3, border_width=2, border_focus='#4e5f60', border_normal='#000000')]



###########
# Taskbar #
###########
widget_defaults = dict(font='Ubuntu Nerd Font', background=c_bar)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widget_separator = 13   # set value for widget.Sep padding
    widgets_list = [
        widget.CurrentScreen(
            background = '#000000',
            active_text='',
            inactive_text='',
            active_color=c_window_name,
            inactive_color=c_active_ws),

        widget.WindowName(
            padding = 0,
            foreground = c_widget_icon,
            max_chars = 40),

        widget.GroupBox(
            background = '#000000',
            font='Ubuntu bold',
            margin_x = 0,
            margin_y=4,
            padding_x = 0,
            spacing = 8,
            fontsize = 13,
            hide_unused=True,
            use_mouse_wheel = False,
            disable_drag = True,
            borderwidth = 1,
            active = c_active_ws,
            inactive = c_inactive_ws,
            highlight_method = 'line',
            block_highlight_text_color = c_active_ws,   # selected text color
            highlight_color = ['000000', '000000'], # active screen and window color gradient
            # current screen view
            this_current_screen_border = '#a6e22e',   # main screen selected
            other_screen_border = '#ae81ff',          # 2nd screen selected
            # other screens view
            other_current_screen_border = '#a6e22e',  # main screen selected
            this_screen_border = '#ae81ff',           # 2nd screen selected
            # urgent
            urgent_alert_method = 'text',
            urgent_text = '#fd971f'),  # highlight workspace with already opened application
        widget.Spacer(length=bar.STRETCH),

        widget.Volume(
            background = '#000000',
            padding = 0,
            fmt='Vol {}',
            # mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('pavucontrol')},  # show volume %
            mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e alsamixer')},
            foreground = c_widget_icon), #ae81ff
        widget.Sep(background='#000000',linewidth=0, padding=widget_separator),
        widget.Backlight(
            background = '#000000',
            padding = 0,
            foreground = c_widget_icon, #66d9ef
            backlight_name = 'intel_backlight', # to get backlight_name: ls /sys/class/backlight/
            format = 'Br {percent:2.0%}',),
        widget.Sep(background='#000000',linewidth=0, padding=widget_separator),
        widget.Battery(
            background = '#000000',
            padding = 0,
            foreground = '#84b424',  #a6e22e
            format='Bat {percent:2.0%}{char}',
            # format='{percent:2.0%}{char}  {hour:d}:{min:02d}  {watt:.0f}W',
            mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' --hold -e upower -i /org/freedesktop/UPower/devices/battery_BAT0')},
            show_short_text=False,
            update_interval=10,
            full_char='',
            charge_char='',
            discharge_char='',
            empty_char='',
            low_percentage=0.1,
            low_foreground='#cc3333'),
        widget.Sep(background='#000000',linewidth=0, padding=widget_separator),
 
        ### For main screen only
        widget.TextBox(
            background = '#000000',
            text = '--|--',
            foreground = '#000000',
            padding = 0),
        widget.KeyboardLayout(
            background = '#000000',
            padding = 0,
            foreground = c_widget_txt,
            configured_keyboards = ['us', 'ua'],
            display_map = {'us': '🇺🇸', 'ua': '🇺🇦'}),
        widget.Systray(background='#000000',icon_size=13, padding=6),
        # widget.StatusNotifier(),
        widget.TextBox(
            background = '#000000',
            text = '--|--',
            foreground = '#000000',
            padding = 0),
        widget.Sep(background='#000000',linewidth=0, padding=widget_separator),
        ###

        widget.Clock(
            background = '#000000',
            padding = 0,
            format = '%Y-%m-%d  %a   %H:%M',
            # format = '%a, %b %d   %H:%M',
            foreground = c_widget_txt,
            mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' --hold -e ./.config/qtile/scripts/show_calendar.sh')}),
    ]
    return widgets_list



###########
# Screens #
##########
# Widgets bar for main monitor
def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

# Widgets bar for another monitors
def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[len(widgets_screen2)-6:len(widgets_screen2)-1]  # remove systray & separator (only one window can function as a systray)
    return widgets_screen2

# Set widget bar for all monitors
screens = [
    Screen(
        wallpaper='~/git/wallpapers/space.png',
        wallpaper_mode='fill',
        top=bar.Bar(widgets=init_widgets_screen1(), background="#000000", opacity=1.0, size=16)),  # 'size' - bar height #wayland
    Screen(
        wallpaper='~/git/wallpapers/space.png',
        wallpaper_mode='fill',
        top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=16)),
]


# Drag floating layouts
mouse = [
    Drag([mod], 'Button1',
         lazy.window.set_position_floating(),
         start=lazy.window.get_position()),  # Move floating window'
    Drag([mod, 'shift'], 'Button1',
         lazy.window.set_size_floating(),
         start=lazy.window.get_size()),      # Resize floating window
    Click([mod], 'Button2',
          lazy.window.bring_to_front())      # Bring to front floating window
]


dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = 'floating_only'
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # Run "xprop | grep 'CLASS'" to see the wm class and name of an X client.
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,

    Match(title='Confirmation'),          # tastyworks exit box
    Match(title='Qalculate!'),            # qalculate-gtk
    Match(wm_class='copyq'),              # clipboard manager
    Match(wm_class='kdenlive'),           # kdenlive
    Match(wm_class='pick-colour-picker'), # Color picker with lens
    Match(wm_class='pinentry-gtk-2'),     # GPG key password entry
], fullscreen_border_width=0, border_width=0)
auto_fullscreen = True
focus_on_window_activation = "smart"  # smart, never, focus
reconfigure_screens = True


# Startup
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/startup_once.sh'])


@hook.subscribe.startup
def start_every_time():
    subprocess.call([home + '/.config/qtile/scripts/startup.sh'])


wmname = 'Qtile [AltOS]'  # 'LG3D' (only for java UI toolkits)

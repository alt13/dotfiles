#!/bin/bash

############
# KEYBOARD #
############
numlockx &
#change your keyboard if you need it
#setxkbmap -layout be
# replaces with qtile widget
# setxkbmap -layout us,ru 					# set keyboard layouts
# setxkbmap -option 'grp:alt_shift_toggle'	# set shortcut fot keyboard layots



############
# UTILITES #
############
# Polkit (polkit-gnome) - allows unprivileged processes to speak to privileged processes
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &	# Arch
/usr/libexec/polkit-gnome-authentication-agent-1 &          # Fedora

nitrogen --restore &  # Restore previous selected wallpaper
# run variety &
nm-applet &
# run pamac-tray &
# blueberry-tray &  # Linux Mint's spin-off of GNOME Bluetooth
# blueman-tray &
picom &					# Compositor for Xorg (Graphical effects)
#light-locker & # screen locker using LightDM to authenticate the user (lock after suspend etc)
dunst &   # lightweight replacement for the notification-daemons provided by most desktop environments.
# notify-send -u low Welcome 'Battlestation ready to rock!' &
# notify-osd &        # Canonical's on-screen-display notification agent
# notify-send Welcome 'Battlestation ready to rock!' &  # Welcome notification
# start the conky
# conky &  # conky -c $HOME/.config/qtile/scripts/system-overview
#unclutter &   # hides your X mouse cursor when you do not need it

# screen lock timeout
# xidlehook \
#  --not-when-fullscreen \
#  --not-when-audio \
#  --timer 18000 "notify-send -u normal 'Locking screen in 10 seconds' -h string:fgcolor:#b2d582" "" \
#  --timer 10 "/home/${USER}/.config/qtile/scripts/i3lock.sh" "" \
#  --timer 3600 "systemctl suspend" "" &
 # --not-when-audio    require pulse audio



################
# APPLICATIONS #
################
redshift-gtk -l 48.379433:31.165581 & 	# Location - ua
# run volumeicon & 	# set volume, cant open alsamixer
# dropbox &
flatpak run com.dropbox.Client &
flatpak run io.gitlab.news_flash.NewsFlash &
#kdeconnect-indicator &
#caffeine & 	# stay awake
#run insync start &
# copyq &  # clipboard manager
# greenclip daemon & 	# demon for greenclip clipboard manager
# /usr/bin/emacs --daemon &  # emacs daemon (speeds up launching emacs)

# Open on workspace
# thunderbird &
# flatpak run org.mozilla.Thunderbird &
# pavucontrol &
# easyeffects &
flatpak run com.github.wwmm.easyeffects &
# sleep 3; qjackctl &



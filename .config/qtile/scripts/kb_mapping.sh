#!/bin/bash
xmodmap -e 'keycode 66 = BackSpace'  # bind Caps Lock
xmodmap -e 'keycode 22 = Caps_Lock'  # bind Backspace
xmodmap -e 'clear lock'  # unbind caps / hold key to erase

# if on default layout backspace deletes only one character > 
# comment/uncomment mapping ralated lines in config > reload
# or "setxkbmap && xmodmap -e 'clear lock'"

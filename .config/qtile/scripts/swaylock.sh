#!/bin/bash
######################################
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################

# Get the current keyboard layout
layout=$(setxkbmap -query | grep layout | awk '{print $2}')

# Lock only on us layout
if [ "$layout" = "us" ]
then
    # Lock
        # --color 000000 `background color` \
        # --image ~/.config/qtile/scripts/img/dots.jpg \
    swaylock \
        --color 000000 `background color` \
        --show-failed-attempts \
        --font "Fira Code NF" `font of the text` \
        --font-size 16 `fixed font size for the indicator text` \
        --indicator-radius 110 `indicator radius` \
        --inside-color ff000000 `color of the inside of the indicator` \
        --inside-clear-color ff000000 `color of the inside of the indicator when cleared` \
        --inside-ver-color ff000000 `color of the inside of the indicator when verifying` \
        --inside-wrong-color ff000000 `color of the inside of the indicator when invalid` \
        --key-hl-color cccccc `color of the key press highlight segments` \
        --show-keyboard-layout `current xkb layout while typing` \
        --layout-text-color cccccc `color of the layout text` \
        --ring-color ff000000 `color of the ring of the indicator when typing or idle` \
        --bs-hl-color cccccc `color of backspace highlight segments` \
        --ring-clear-color ff000000 `color of the ring of the indicator when cleared` \
        --ring-ver-color cccccc `color of the ring of the indicator when verifying` \
        --ring-wrong-color ff8080 `color of the ring of the indicator when invalid` \
        --text-ver-color cccccc `color of the text when verifying`
fi

#000000
#cccccc
#3d3d3d
#d2ff4c
#ff8080

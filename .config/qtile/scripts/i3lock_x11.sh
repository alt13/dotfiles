#!/bin/bash
######################################
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################

# Get the current keyboard layout
layout=$(setxkbmap -query | grep layout | awk '{print $2}')

# Lock only on us layout
if [ "$layout" = "us" ]
then
    # Lock
    i3lock \
        --time-str="%H:%M"\
        --time-size=60\
        --date-str="%a, %b %d"\
        --date-size=20\
        --keylayout 2\
        --verif-text="Authorization"\
        --wrong-text="DENIED"\
        --ignore-empty-password\
        --show-failed-attempts\
        --radius=110\
        --color=000000\
        --ring-color=000000\
        --line-color=cccccc\
        --insidever-color=000000\
        --ringver-color=cccccc\
        --keyhl-color=cccccc\
        --bshl-color=3d3d3d\
        --separator-color=3d3d3d\
        --insidewrong-color=000000\
        --ringwrong-color=ff8080\
        --verif-color=cccccc\
        --wrong-color=ff8080\
        --layout-color=cccccc\
        --time-color=cccccc\
        --date-color=cccccc
fi

#000000
#cccccc
#3d3d3d
#d2ff4c
#ff8080

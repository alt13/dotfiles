#!/bin/bash
###################################### Inspired and based on:
#     ___   _    _____ __   _____    # https://gitlab.com/Nmoleo/i3-volume-brightness-indicator
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################

notification_timeout=1000
bar_color="#387db7"


# Uses regex to get volume from pactl
function get_volume {
    pactl get-sink-volume @DEFAULT_SINK@ | grep -Po '[0-9]{1,3}(?=%)' | head -1
}

# Uses regex to get mute status from pactl
function get_mute {
    pactl get-sink-mute @DEFAULT_SINK@ | grep -Po '(?<=Mute: )(yes|no)'
}

# Uses regex to get brightness from xbacklight
function get_brightness {
    light | grep -Po '[0-9]{1,3}' | head -n 1
}

# Returns a mute icon, a volume-low icon, or a volume-high icon, depending on the volume
function get_volume_icon {
    volume=$(get_volume)
    mute=$(get_mute)
    if [ "$volume" -eq 0 ] || [ "$mute" == "yes" ] ; then
        volume_icon="MUTED"
    else
        volume_icon="VOLUME"
    fi
}

function get_brightness_icon {
    brightness_icon="BRIGHTNESS"
}

function show_volume_notif {
    volume=$(get_mute)
    get_volume_icon
    notify-send -t $notification_timeout -h string:x-dunst-stack-tag:volume_notif -h string:hlcolor:"#8b67cc" -h int:value:$volume "$volume_icon $volume%"
}

# Displays a brightness notification using dunstify
function show_brightness_notif {
    brightness=$(get_brightness)
    echo $brightness
    get_brightness_icon
    notify-send -t $notification_timeout -h string:x-dunst-stack-tag:brightness_notif -h string:hlcolor:"#51adbf" -h int:value:$brightness "$brightness_icon $brightness%"
}

# Main function - Takes user input, "volume_up", "volume_down", "brightness_up", or "brightness_down"
case $1 in
    volume_up)
    show_volume_notif
    ;;

    volume_down)
    show_volume_notif
    ;;

    volume_mute)
    show_volume_notif
    ;;

    brightness_up)
    show_brightness_notif
    ;;

    brightness_down)
    show_brightness_notif
    ;;
esac

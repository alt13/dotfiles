#!/usr/bin/env bash
set -euo pipefail

date=$(date +"%A, %B %d")
eu_date=$(date +"%Y-%m-%d")
us_date=$(date +"%m/%d/%Y")

# echo "___________________________________________________________________________"
# echo "                                  CALENDAR                                 "
# echo "$eu_date                                                       $us_date"
# echo "YYYY-MM-DD [eu] ISO 8601                                    [us] MM/DD/YYYY" 
# echo "___________________________________________________________________________"
cal -y -w
# echo "___________________________________________________________________________"
# notify-send -a Calendar -u critical "$date"

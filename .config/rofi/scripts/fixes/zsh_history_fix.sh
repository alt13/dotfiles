#!/usr/bin/env zsh
# https://shapeshed.com/zsh-corrupt-history-file/

mv ~/.cache/zshhistory ~/.cache/zshhistory_bad
strings ~/.cache/zshhistory_bad > ~/.cache/zshhistory 
fc -R ~/.cache/zshhistory
rm ~/.cache/zshhistory_bad


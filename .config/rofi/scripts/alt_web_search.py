###################################### 
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################
# https://kcode.co.za/how-to-make-a-command-menu-in-python-using-rofi/

import os
import re


search = '$(echo "" | rofi -dmenu -no-fixed-num-lines -p " ")'
web_search = {
    # Search engines
    'g' :  'https://www.google.com/search?q=',
    'gm':  'https://www.google.com/maps/search/',
    'gt':  'https://translate.google.as/?sl=en&tl=ru&text=',
    'ddg': 'https://www.duckduckgo.com/?q=',
    's':   'https://www.startpage.com/search?q=',
    # Social
    'yt':  'https://www.youtube.com/results?search_query=',
}


# run command and read output
def rofi(command): 
    return os.popen(command).read()[:-1]


def rofi_web_search(key, usr_query):
    rofi('xdg-open "' + web_search[key] + '"' + usr_query)


def menu():
    usr_input = rofi('rofi -dmenu -no-fixed-num-lines -p " "')

    # get first word 
    all_words = usr_input.split()

    # if not empmty
    if all_words:
        first_word = all_words[0]

    # close if empty input
    if not all_words:
        pass
    # if more then one word & first word is a key
    elif len(all_words) > 1 and first_word in web_search:
        # remove key from a search
        search(first_word, usr_input.split(' ', 1)[1])
    else:
        # use Google by default
        search('g', usr_input)


def search(key, request):
    # replace multiple spaces with a '+'
    request = '+'.join(request.split())

    # replace special characters wih URL codes 
    chars = {
        '#': '%23',
        '&': '%26',
        "'": '%27',
        '(': '%28',
        ')': '%29',
        '++': '+%2B',
        '<': '%3C',
        '>': '%3E',
        '`': '%60',
        '|': '%7C',
    }
    for c in chars:
        request = request.replace(c, chars[c])

    # browser search
    rofi('xdg-open "' + web_search[key] + request + '"')


menu()


###################################### 
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################
# https://kcode.co.za/how-to-make-a-command-menu-in-python-using-rofi/

import os

opener = 'xdg-open'
terminal = 'kitty'
editor = 'kitty -e nvim'

# Shortcuts
config = ''
cheatsheet = ''
script = ''
bookmark = ''
link = ''

commands = [

    #################
    # Web bookmarks #
    #################
    # Search engine
    {'name': f'{link} g (Google)',                     'run': f'{opener} "https://www.google.com/"'},
    {'name': f'{link} Google translate eng - ru',      'run': f'{opener} "https://translate.google.as/?sl=en&tl=ru&op=translate"'},
    {'name': f'{link} ddg (DuckDuckGo)',               'run': f'{opener} "https://www.duckduckgo.com/"'},

    # AI
    {'name': f'{link} OpenAI > ChatGPT',               'run': f'{opener} "https://chat.openai.com/chat"'},

    # Social
    {'name': f'{link} Telegram Web',                   'run': f'{opener} "https://web.telegram.org/"'},
    {'name': f'{link} WhatsApp Web',                   'run': f'{opener} "https://web.whatsapp.com/"'},
    {'name': f'{link} yt (YouTube)',                   'run': f'{opener} "https://www.youtube.com/"'},
    {'name': f'{link} Reddit',                         'run': f'{opener} "https://reddit.com/"'},
    {'name': f'{link} Discord',                        'run': f'{opener} "https://discord.com/app"'},
    {'name': f'{link} Odysee',                         'run': f'{opener} "https://odysee.com/"'},
    # Torrent
    {'name': f'{link} [torrent] The Pirate Bay',       'run': f'{opener} "https://thepiratebay.org/index.html"'},
    {'name': f'{link} [torrent] BabyTorrent',          'run': f'{opener} "https://babytorrent.fun/"'},
    # Media
    {'name': f'{link} Goodreads',                      'run': f'{opener} "https://www.goodreads.com"'},
    # Other
    {'name': f'{link} GitLab',                         'run': f'{opener} "https://gitlab.com/dashboard/projects"'},
    {'name': f'{link} [weather] Meteorological radar', 'run': f'{opener} "https://radar.veg.by/kiev/"'},
    {'name': f'{link} [weather] Weawow',               'run': f'{opener} "https://weawow.com/"'},


    ####################
    # System bookmarks #
    ####################
    # Configs
    {'name': f'{config} Configs',                      'run': f'{terminal} /home/$USER/.config/'},
    {'name': f'{config} Flatpak configs',              'run': f'{terminal} /home/$USER/.var/app/'},
    {'name': f'{config} Default apps (mimeapps.list)', 'run': f'{editor} "/home/$USER/.config/mimeapps.list"'},
    {'name': f'{config} Alt menu',                     'run': f'{editor} "/home/$USER/.config/rofi/scripts/alt_menu.py"'},
    {'name': f'{config} Alt OS',                       'run': f'{editor} "/home/$USER/git/os/arch/setup_arch.sh"'},
    {'name': f'{config} Alt web search',               'run': f'{editor} "/home/$USER/.config/rofi/plugins/alt_web_search.py"'},
    {'name': f'{config} Qtile',                        'run': f'{editor} "/home/$USER/.config/qtile/config.py"'},
    {'name': f'{config} Hyprland',                     'run': f'{editor} "/home/$USER/.config/hypr/hyprland.conf"'},
    {'name': f'{config} Startup apps',                 'run': f'{editor} "/home/$USER/.config/qtile/scripts/startup_once.sh"'},
    {'name': f'{config} Alt wiki',                     'run': f'{editor} "/home/alt/git/wiki/"'},
    {'name': f'{config} GRUB config',                  'run': f'{opener} "/etc/default"'},
    {'name': f'{config} Themes directory',             'run': f'{terminal} "/home/$USER/.local/share/themes/"'},
    {'name': f'{config} Icons directory',              'run': f'{terminal} "/home/$USER/.local/share/icons/"'},
    {'name': f'{config} Fonts directory',              'run': f'{terminal} "/home/$USER/.local/share/fonts/"'},


    # Cheat sheets
    {'name': f'{cheatsheet} Wiki',                  'run': f'{terminal} /home/$USER/git/wiki'},
    {'name': f'{cheatsheet} Alt13 keyboard layout', 'run': f'{opener} ~/.config/rofi/scripts/img/alt13_kb.png'},
    # {'name': f'{cheatsheet} Tmux',                  'run': f'{terminal} {editor} "/home/$USER/git/wiki/Apps/tmux.org"'},
    # {'name': f'{cheatsheet} Neovim (nvim)',         'run': f'{terminal} {editor} "/home/$USER/git/wiki/nvim.org"'},

    # Bookmarks
    {'name': f'{bookmark} Notes',        'run': f'{editor} "/home/$USER/Dropbox/notes.org"'},
    {'name': f'{bookmark} Watching list','run': f'{editor} "/home/$USER/Dropbox/watching_list.org"'},
    {'name': f'{bookmark} Reading list', 'run': f'{editor} "/home/$USER/Dropbox/reading_list.txt"'},


    ###########
    # Scripts #
    ###########
    {'name': f'{script} Restart Qtile',                         'run': f'qtile cmd-obj -o cmd -f restart'},

    # Monitors 
    # XORG
    # xrandr --output eDP-1 --primary --output HDMI-1-0 --mode 2560x1440 --rate 144 --left-of eDP-1
    # {'name': f'{script} Connect HDMI screen',                   'run': f'xrandr --output HDMI-1-1 --primary --mode 1920x1080 --left-of eDP-1'},  # Debian
    # {'name': f'{script} Disconnect HDMI screen',                'run': f'xrandr --auto && xrandr --output HDMI-1-1 --off'}, # Debian
    # {'name': f'{script} Connect HDMI screen',                   'run': f'RESTART_QTILE=$(qtile cmd-obj -o cmd -f restart) && xrandr --output eDP-1 --primary --output HDMI-1-0 --mode 1920x1080 --right-of eDP-1 && nitrogen --restore && $RESTART_QTILE'},   # Arch
    # {'name': f'{script} Connect HDMI 2K 144Hz screen',            'run': f'RESTART_QTILE=$(qtile cmd-obj -o cmd -f restart) && xrandr --output eDP-1 --primary --output HDMI-1-0 --mode 2560x1440 --rate 144 --left-of eDP-1 && nitrogen --restore && $RESTART_QTILE'},   # Arch
    # {'name': f'{script} Connect HDMI screen',                   'run': f'xrandr --output HDMI-1-0 --primary --mode 1920x1080 --left-of e-DP1'},  # Fedora
    # {'name': f'{script} Disconnect HDMI screen',                'run': f'xrandr --auto && xrandr --output HDMI-1-0 --off'},
    # WAYLAND (Hyprland)
    {'name': f'{script} Connect HDMI screen',                   'run': f'hyprctl dispatch dpms on HDMI-A-1'},
    {'name': f'{script} Disconnect HDMI screen',                'run': f'hyprctl dispatch dpms off HDMI-A-1'},

    # Keyboard
    {'name': f'{script} Mapping for default keyboard',          'run': f'/home/$USER/.config/qtile/scripts/kb_mapping.sh'},
    {'name': f'{script} Reset keyboard mapping',                'run': f'setxkbmap && xmodmap -e "clear lock"'},

    # GPU
    {'name': f'{script} GPU switch mode [Integrated]',          'run': f'{terminal} sudo envycontrol -s integrated'},
    {'name': f'{script} GPU switch mode [Hybrid]',              'run': f'{terminal} sudo envycontrol -s hybrid'},
    {'name': f'{script} GPU switch mode [Nvidia] (X.org only)', 'run': f'{terminal} sudo envycontrol -s nvidia'},
    {'name': f'{script} Show active GPU',                       'run': f'ACTIVE_GPU=$(echo "[ACTIVE GPU]\n" && glxinfo|egrep "OpenGL vendor|OpenGL renderer") && {terminal} --hold -e echo "$ACTIVE_GPU"'},
    {'name': f'{script} nvtop - GPU activity',                  'run': f'{terminal} nvtop'},

    # System info
    {'name': f'{script} Swap usage <free -h>',                  'run': f'{terminal} --hold -e free -h'},
    {'name': f'{script} Space usage <df -h> or <duf>',          'run': f'{terminal} --hold -e duf'},

    {'name': f'{script} Wake home server',                      'run': f'wol 00:24:54:ae:52:61 && sleep 20 && ssh -i ~/.ssh/home_server alt@192.168.1.2 sudo light -S 0'},
    {'name': f'{script} Suspend home server',                   'run': f'ssh -i ~/.ssh/home_server alt@192.168.1.2 sudo systemctl suspend'},

    {'name': f'{script} Start emacs daemon',                    'run': f'/usr/bin/emacs --daemon &'},
    {'name': f'{script} Restart emacs daemon',                  'run': f'killall emacs && /usr/bin/emacs --daemon &'},

    {'name': f'{script} Start mumble server',                   'run': f'systemctl start murmur'},
    {'name': f'{script} Stop mumble server',                    'run': f'systemctl stop murmur'},

    {'name': f'{script} Speed test',                            'run': f'{terminal} --hold -e speedtest'},
    {'name': f'{script} Current screen resolution',             'run': f'RESOLUTION=$(echo "[CURRENT SCREEN RESOLUTION]\n" && xrandr | grep "*") && {terminal} --hold -e echo "$RESOLUTION"'},

    {'name': f'{script} libvert - start default network',       'run': f'{terminal} sudo virsh net-start default'},

    {'name': f'{script} scrcpy - mirror android screen',        'run': f'{terminal} scrcpy --tcpip=192.168.1.177'},

    # Services
    {'name': f'{script} Start bluetooth service',               'run': f'systemctl start bluetooth.service'},

    # Misc
    {'name': f'{script} Regenerate font cache',                 'run': f'{terminal} fc-cache -f -v'},

    # Fixes
    {'name': f'{script} Fix a corrupt zsh history file',        'run': f'{terminal} /home/$USER/.config/rofi/scripts/fixes/zsh_history_fix.sh'},
]


# run command and read output
def rofi(command): 
    return os.popen(command).read()[:-1]


def menu():

    combined_dic = {}
    command_names = []
    for c in commands:
        combined_dic[c['name']] = c['run']
        command_names.append(c['name'])

    # Run selected
    # ROFI
    # command = 'echo "{}" | rofi -dmenu -no-fixed-num-lines -i -p " Λlt13 "'.format("\n".join(command_names))
    # TOFI
    command = 'echo "{}" | tofi'.format("\n".join(command_names))

    usr_input = rofi(command)

    # Run action
    if usr_input != '':
        action = combined_dic[usr_input]

        if type(action) == list:
            # if it is a nested menu, we call the menu function recursively 
            menu(action, combined_dic[usr_input])
        else:
            # execute the action
            os.system(action)

menu()


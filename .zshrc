blue='\e[34m%s\e[0m%s'
green='\e[32m%s\e[0m%s'



#
# LOGIN MANAGER
# https://www.youtube.com/watch?v=dtuy09mqBPI&t=342s
if [[ "$(tty)" = "/dev/tty1" ]]; then
    pgrep || Hyprland
    # XDG_SESSION_TYPE=wayland dbus-run-session gnome-session
    # qtile start -b wayland
    # startx
    # startplasma-wayland
fi

#
# KYES
#
# bindkey "^U" backward-kill-line  # fix Ctrl + U deletes whole line regardless of cursor position

#
# PATH
#

#
# ENVIRONMENT VARIABLES
#
# export XCURSOR_THEME=Bibata-Modern-Ice
# export XCURSOR_THEME=Bibata-Modern-Ice  # DELETE
export XDG_CURRENT_DESKTOP=Hyprland     # flatpak cursor theme doen't work without it
export WLR_NO_HARDWARE_CURSORS=1
export EDITOR='nvim'
export TERMINAL='kitty'
export OPENER='xdg-open'
# export BROWSER='brave'
# export VIDEO='mpv'
export FILEMANAGER='nemo'

# User system bookmarks
export music='/media/enc_hdd/Music'
export downloads='/media/enc_hdd/Downloads'
export torrents='/media/enc_hdd/Downloads/Torrents'

alias hdd='cd /mnt/enc_hdd/'

#
# ALIASES
#

# Apps
alias v='nvim'
alias sv='sudoedit'  # require env for editor
# creates temp file and replace original only after edit is finished.

# Terminal soft alternatives
alias ls='lsd'
alias ll='lsd -l'
j() {              # jump + ls
    __zoxide_z $@  # zoxide
    ls
}
cd() {             # cd + ls
    builtin cd $@
    ls
}

alias cat='bat'  # bat --plain
alias grep='batgrep'
alias Man='batman'
alias watch='batwatch'
alias diff='batdiff'

alias df='duf'
alias find='fd'
alias man='tldr'
# alias Man='/usr/bin/man' # defaul man

# cd() {cd "$@" && ls} # cd + ls (example)

alias wifi='nmtui'

# ARCH
alias install='paru -S'                           # arch install
alias remove='sudo pacman -Rns'                          # arch remove
update() {
    # deb_upd='sudo apt update & sudo apt -y upgrade & sudo apt autoremove & sudo apt clean'
    echo
    printf $blue "SYSTEM UPDATE"
    echo
    printf $green "Update Flatpacks? [y/n]: "
    while true; do
        read answer
        if [ "$answer" = "y" ]; then
            paru -Syu && flatpak update -y
        fi

        if [ "$answer" = "n" ]; then
            paru -Syu
        fi
        break
    done
}
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'  # arch remove orphaned packages
alias mirrors='sudo reflector --fastest 10 --latest 10 --sort rate --verbose --save /etc/pacman.d/mirrorlist' # get fastest mirrors

# FEDORA
# alias i='sudo dnf install '
# alias r='sudo dnf remove '
# alias update='sudo dnf update && flatpak update'
# alias cleanup='sudo dnf autoremove'

# DEBIAN
#

alias :q='exit'
alias zsh='source ~/.zshrc'  # reload ZSH config
alias fonts='fc-cache -vf'   # scans the font directories on the system and builds font information cache
alias hist='xdg-open ~/.cache/zshhistory'  # open zsh history
alias history='history -i 0'  # HISTTIMEFORMAT + show all  

alias out='loginctl terminate-session ${XDG_SESSION_ID-}'

alias ssh='env TERM=xterm-256color ssh'
alias ssha='eval $(ssh-agent) && ssh-add'  # cache passphare to avoid typing it every time

alias e="$FILEMANAGER & disown"

# Alias for git bare dotfiles backup
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"

alias cp='cp -r'     # recursive copy
alias mv='mv -v'     # show info
# alias rm='trash -v'  # replace with trash-cli

# Colorize grep output (good for log files)
# alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# creates directory recursively
alias mkdir='mkdir --parents --verbose'

# navigation
alias ..='cd ..'
alias .1='cd ..'
alias .2='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# open file with default app
open() {$OPENER $* & disown}
# audacity &> /dev/null &
# find & change directory
fcd() {cd "$(find -type d | fzf)"}
# find & open the file 
Open() {$OPENER "$(find -type f | fzf)"}
tes() {find -type f | fzf | echo}
# find & copy file path to clipboard
alias getpath="find -type f | fzf | sed 's/^..//' | tr -d '\n' | xclip -selection c"

# detect outdated shared libraries still linked to processes in memory
alias uchecker='curl -s -L https://kernelcare.com/uchecker | sudo python'

# different
alias speedtest='curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 -'
alias weather='curl wttr.in'
help() { curl --max-time 5 "http://cheat.sh/$1"; }

#
# SETTINGS
#

# Set vi mode
# bindkey -e  # emacs mode (default)
bindkey -v  # vi mode

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zshhistory
setopt appendhistory

# Basic auto/tab complete: (use tab for visual select)
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)               # Include hidden files.

#
# THEME
#
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
source ~/.powerlevel10k/powerlevel10k.zsh-theme
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#
# ZSH PLUGINS (should be last)
#
# source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null                       # FEDORA
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null           # ARCH
# source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null               # FEDORA
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null   # ARCH
eval "$(zoxide init zsh)"

if [ -e /home/alt/.nix-profile/etc/profile.d/nix.sh ]; then . /home/alt/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
